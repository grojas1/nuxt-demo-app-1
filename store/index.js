export const store = () => ({
  token: null,
  contents: [],
  content: {}
});

export const getters = {
  token: state => state.token,
  contents: state => state.contents,
  content: state => state.content
};

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token;
  },
  SET_CONTENTS(state, contents) {
    state.contents = contents;
  },
  SET_CONTENT(state, content) {
    state.content = content;
  }
};

export const actions = {
  async getToken({ commit }) {
    const config = {
      headers: { 'content-type': 'application/x-www-form-urlencoded' }
    };

    const clientSecret = 'bvetaa5kttwgolcb0fwhbocc4z0i7kbcm1q0kfdx1kyx';
    const clientId = 'app-test-1:default';
    const data = `scope=squidex-api&client_secret=${clientSecret}&client_id=${clientId}&grant_type=client_credentials`;

    const url = 'https://cloud.squidex.io/identity-server/connect/token';
    const response = await this.$axios.post(url, data, config);

    // console.log(response.data.access_token);
    commit('SET_TOKEN', response.data.access_token);
  },
  async getContents(context) {
    const { commit, state } = context;

    const appName = 'app-test-1';
    const schemaName = 'schema3';
    const url = `/content/${appName}/${schemaName}/`;

    const token = state.token;
    const config = {
      headers: { Authorization: `Bearer ${token}` }
    };

    const response = await this.$axios.get(url, config);
    const contents = response.data.items;
    // console.log(contents);
    commit('SET_CONTENTS', contents);
  },
  async getContent(context, id) {
    const { commit, state } = context;
    const token = state.token;
    const config = {
      headers: { Authorization: `Bearer ${token}` }
    };

    const appName = 'app-test-1';
    const schemaName = 'schema3';
    const url = `/content/${appName}/${schemaName}/${id}`;
    const response = await this.$axios.get(url, config);
    const content = response.data.data;
    // console.log(content);
    commit('SET_CONTENT', content);
  }
};
